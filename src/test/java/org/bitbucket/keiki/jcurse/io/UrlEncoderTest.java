package org.bitbucket.keiki.jcurse.io;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

import org.junit.Test;

public class UrlEncoderTest {

    @Test
    public void encodeUrl() throws MalformedURLException, URISyntaxException {
        String encodedUrl = UrlEncoder.encode("http://addons.curse.cursecdn.com/files/2367/444/" +
                "Altoholic v7.1.004.zip").toString();
        assertEquals("http://addons.curse.cursecdn.com/files/2367/444/Altoholic%20v7.1.004.zip", encodedUrl);
    }

}
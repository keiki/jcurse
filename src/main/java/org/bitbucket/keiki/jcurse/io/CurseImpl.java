package org.bitbucket.keiki.jcurse.io;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.bitbucket.keiki.jcurse.io.Constants.USER_AGENT;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.keiki.jcurse.data.Addon;
import org.bitbucket.keiki.jcurse.data.BusinessException;
import org.bitbucket.keiki.jcurse.data.ReleaseStatus;
import org.bitbucket.keiki.jcurse.io.downloadurl.CurseBetaAlphaUrlGetter;
import org.bitbucket.keiki.jcurse.io.downloadurl.CurseForge;
import org.bitbucket.keiki.jcurse.io.downloadurl.CurseStableUrlGetter;
import org.bitbucket.keiki.jcurse.io.downloadurl.UrlGetter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CurseImpl implements Curse {
    

    private static final int URL_ID_PART_END = 2;
    private static final int URL_ID_PART_START = 3;

    private static final Logger LOG = LoggerFactory.getLogger(CurseImpl.class);
    private AddonFileHandler folderHandler;
    
    public CurseImpl(String addonFolderName) {
        this.folderHandler = new AddonFileHandler(addonFolderName);
    }
    
    @Override
    public void downloadToWow(Addon newAddon, String downloadUrl) {
        Set<String> addonFolders = downloadAndExtract(downloadUrl, true);
        String[] split = StringUtils.split(downloadUrl, '/');
        String zipFilename = split[split.length - 1];
        int fileId = extractFileId(split);
        newAddon.setVersionId(fileId);
        newAddon.setLastZipFileName(zipFilename);
        newAddon.setFolders(addonFolders);
    }
    
    
    private boolean downloadToWow(Addon newAddon) {
        try {
            String downloadUrl = getDownloadUrl(newAddon);
            downloadToWow(newAddon, downloadUrl);
            return true;
        } catch (NoSuchElementException e) {
            LOG.warn("No addon found with the name '" + newAddon.getAddonNameId() + "'. Skipping.");
            LOG.debug("No addon found", e);
        }
        return false;
    }

    protected Set<String> downloadAndExtract(String downloadUrl, boolean encodeUrl) {
        try {
            URL website = encodeUrl ? UrlEncoder.encode(downloadUrl) : new URL(downloadUrl);
            URLConnection connection = website.openConnection();
            connection.setRequestProperty("User-Agent", USER_AGENT);

            String location = connection.getHeaderField("Location");
            if (isEmpty(location)) {
                return folderHandler.download(connection.getInputStream());
            } else {
                return downloadAndExtract(location, false);
            }

        } catch (IOException e) {
            throw new BusinessException("Problems reading data from Curse." +
                    " If this issue persists please create a bug.", e);
        }
    }



    public static String extractZipFileName(String downloadUrl) {
        int lastIndexOf = downloadUrl.lastIndexOf('/');
        if (lastIndexOf == -1) {
            throw new BusinessException("Download url wrong");
        }
        return downloadUrl.substring(lastIndexOf + 1);
    }
    
    public static int extractFileId(String[] split) {
        String join = StringUtils.join(split[split.length-URL_ID_PART_START],
                split[split.length-URL_ID_PART_END]);
        return Integer.parseInt(join);
    }

    @Override
    public String getDownloadUrl(Addon addon) {
        if (addon.getReleaseStatus() == ReleaseStatus.RELEASE) {
            return new CurseStableUrlGetter().getDownloadUrl(addon).get();
        }
        UrlGetter curseForge = new CurseForge();
        UrlGetter mainBetaAlpha = new CurseBetaAlphaUrlGetter();

        CompletableFuture<Optional<String>> future = CompletableFuture.supplyAsync(() -> curseForge.getDownloadUrl(addon));

        Optional<String> curseMainUrl = mainBetaAlpha.getDownloadUrl(addon);

        Optional<String> curseForgeUrl;
        try {
            curseForgeUrl = future.get();
        } catch (InterruptedException | ExecutionException e) {
            LOG.warn("Error calling curse forge", e);
            throw new IllegalStateException(e);
        }
        if (curseForgeUrl.isPresent()) {
            return curseForgeUrl.get();
        }
        if (curseMainUrl.isPresent()) {
            return curseMainUrl.get();
        }
        throw new IllegalStateException("No download url found");

    }


    @Override
    public void removeAddons(Collection<Addon> toDelete) {
        for (Addon addon : toDelete) {
            removeAddon(addon);
        }
    }
    
    @Override
    public void removeAddon(Addon toDelete) {
        folderHandler.removeAddonFolders(toDelete.getFolders());
    }

    @Override
    public List<Addon> downloadToWow(List<Addon> toDownload) {
        List<Addon> downloadedAddons = new ArrayList<>();
        for (Addon addon : toDownload) {
            LOG.info("adding " + addon.getAddonNameId());
            if (downloadToWow(addon)) {
                downloadedAddons.add(addon);
                LOG.info("addon " + addon.getAddonNameId() + " was added");
            }
        }
        return downloadedAddons;
    }
}

package org.bitbucket.keiki.jcurse.io;

import org.bitbucket.keiki.jcurse.data.BusinessException;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

class UrlEncoder {

    static URL encode(String urlToEncode) {
        try {
            URL url = new URL(urlToEncode);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(),
                    url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            return uri.toURL();
        } catch (MalformedURLException | URISyntaxException e) {
            throw new BusinessException("Can't encode URL " + urlToEncode, e);
        }
    }
}

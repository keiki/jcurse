package org.bitbucket.keiki.jcurse.io.downloadurl;

import java.util.Optional;
import org.bitbucket.keiki.jcurse.data.Addon;

public class CurseStableUrlGetter implements UrlGetter {

    private static final String CURSE_BASE_URL = "http://www.curse.com/addons/wow/";

    @Override
    public Optional<String> getDownloadUrl(Addon addon) {
        String url = CURSE_BASE_URL + addon.getAddonNameId() + "/download";

        return CurseDownloadSiteUrlGetter.getDownloadUrlFromDownloadSite(url, addon);
    }
}

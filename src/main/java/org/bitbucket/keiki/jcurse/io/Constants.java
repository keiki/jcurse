package org.bitbucket.keiki.jcurse.io;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public final class Constants {
    public static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) " +
            "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36";
    public static final Charset CHARSET_WEBSITE = StandardCharsets.UTF_8;

    
    private Constants() {
    }
}

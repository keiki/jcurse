package org.bitbucket.keiki.jcurse.io.downloadurl;

import java.util.Optional;
import org.bitbucket.keiki.jcurse.data.Addon;

public interface UrlGetter {

    Optional<String> getDownloadUrl(Addon addon);
}

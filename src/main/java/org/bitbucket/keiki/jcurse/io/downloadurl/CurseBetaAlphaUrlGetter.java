package org.bitbucket.keiki.jcurse.io.downloadurl;

import java.io.IOException;
import java.util.Optional;
import org.bitbucket.keiki.jcurse.data.Addon;
import org.bitbucket.keiki.jcurse.data.ReleaseStatus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CurseBetaAlphaUrlGetter implements UrlGetter {

    private static final Logger LOG = LoggerFactory.getLogger(CurseBetaAlphaUrlGetter.class);
    private static final String CURSE_BASE_URL = "http://www.curse.com";


    @Override
    public Optional<String> getDownloadUrl(Addon addon) {

        Optional<String> downloadSiteUrl = getDownloadSiteUrl(addon);
        if (downloadSiteUrl.isPresent()) {
            return CurseDownloadSiteUrlGetter.getDownloadUrlFromDownloadSite(downloadSiteUrl.get(), addon);
        }
        return Optional.empty();
    }

    private Optional<String> getDownloadSiteUrl(Addon addon) {
        try {
            Document doc = Jsoup.connect("https://mods.curse.com/addons/wow/gnomishvendorshrinker").get();
            Elements elements = doc.select("#tab-other-downloads > div > div.listing-body > table > tbody > tr");

            for (Element element : elements) {
                String text = element.child(1).text();
                if (ReleaseStatus.valueOfIgnoreCase(text) == addon.getReleaseStatus()) {
                    Element downloadVersionTag = element.child(0);
                    String subLink = downloadVersionTag.getElementsByTag("a").attr("href");
                    return Optional.of(CURSE_BASE_URL + subLink);
                }
            }

            return Optional.empty();

        } catch (IOException e) {
            LOG.error("error accessing mods site", e);
            return Optional.empty();
        }
    }
}

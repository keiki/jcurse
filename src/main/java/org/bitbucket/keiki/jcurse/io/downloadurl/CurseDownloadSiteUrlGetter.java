package org.bitbucket.keiki.jcurse.io.downloadurl;

import static org.bitbucket.keiki.jcurse.io.Constants.CHARSET_WEBSITE;
import static org.bitbucket.keiki.jcurse.io.Constants.USER_AGENT;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.bitbucket.keiki.jcurse.data.Addon;
import org.bitbucket.keiki.jcurse.data.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CurseDownloadSiteUrlGetter {

    private static final String HTML_ATTRIBUTE_DOWN_URL = "data-href";
    private static final String HTML_ATTRIBUTE_ADDON_ID = "data-project";

    private static final Logger LOG = LoggerFactory.getLogger(CurseDownloadSiteUrlGetter.class);


    public static Optional<String> getDownloadUrlFromDownloadSite(String url, Addon addon) {
        try {
            LOG.debug("accessing {}", url);
            HttpClient httpClient = new HttpClient();
            GetMethod method = new GetMethod(url);
            method.setRequestHeader("user-agent", USER_AGENT);
            int status = executeHttpMethod(httpClient, method);
            LOG.debug("state {}", status);
            String downloadUrl;
            try (BufferedReader reader = new BufferedReader
                    (new InputStreamReader(getDownloadStream(method), CHARSET_WEBSITE))) {
                downloadUrl = readLines(addon, reader);
            }
            if (downloadUrl.isEmpty()) {
                LOG.info("Addon couldn't be found on curse. If this issue persists create a  bug ticket.");
                return Optional.empty();
            }
            return Optional.of(downloadUrl);
        } catch (IOException e) {
            throw new BusinessException("Can't access " + url, e);
        } catch (NumberFormatException e) {
            throw new BusinessException("Can't parse addon numerical id.", e);
        }
    }

    private static String readLines(Addon addon, BufferedReader reader) throws IOException {
        String line;
        while ((line = reader.readLine()) != null) {
            if (addon.getAddonId() == 0) {
                String parseAttribute = WebsiteHelper.parseAttribute(line, HTML_ATTRIBUTE_ADDON_ID);
                if (!parseAttribute.isEmpty()) {
                    addon.setAddonId(Integer.parseInt(parseAttribute));
                }
            }
            String downloadUrl = WebsiteHelper.parseAttribute(line, HTML_ATTRIBUTE_DOWN_URL);
            if (addon.getAddonId() != 0 && !downloadUrl.isEmpty()) {
                return downloadUrl;
            }
        }
        return "";
    }


    protected static InputStream getDownloadStream(GetMethod method)
            throws IOException {
        return method.getResponseBodyAsStream();
    }

    protected static int executeHttpMethod(HttpClient httpClient, GetMethod method)
            throws IOException {
        return httpClient.executeMethod(method);
    }

}
